//
//  UIStackView+Extensions.swift
//  RedditTestApp
//
//  Created by Serhii Oleinich on 20.02.2021.
//

import UIKit

extension UIStackView {
    convenience init(closure: (UIStackView) -> Void) {
        self.init()
        
        closure(self)
    }
    
    @discardableResult
    public func spinner(apply closure: (UIActivityIndicatorView) -> Void) -> UIActivityIndicatorView {
        let indicatorView = UIActivityIndicatorView(style: .medium)
        addArrangedSubview(indicatorView)
        closure(indicatorView)
        return indicatorView
    }
}

//
//  UIBarButtonItem+Extensions.swift
//  RedditTestApp
//
//  Created by Serhii Oleinich on 11.02.2021.
//

import UIKit

extension UIBarButtonItem {
    var isHidden: Bool { self.tintColor == .clear }
    
    var isLoading: Bool {
        get { self.customView is UIStackView }
        set {
            guard isLoading != newValue else {
                return
            }
            
            if newValue {
                self.customView = {
                    let stackView: UIStackView = .init {
                        $0.axis = .horizontal
                        $0.isLayoutMarginsRelativeArrangement = true
                        $0.layoutMargins = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 16)
                        
                        $0.spinner {
                            $0.startAnimating()
                        }
                    }
                    
                    return stackView
                }()
            } else {
                self.customView = nil
            }
        }
    }
}

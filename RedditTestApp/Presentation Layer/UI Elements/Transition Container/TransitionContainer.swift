//
//  TransitionContainer.swift
//  RedditTestApp
//
//  Created by Serhii Oleinich on 08.02.2021.
//

import UIKit

protocol TransitionContainer {
    func present(viewController: UIViewController)
    func presentEmbededInNavigationController(viewController: UIViewController)
    
    func push(_ viewController: UIViewController)
    
    func pop()
}

struct TransitionContainerImpl<Context>: TransitionContainer {
    let context: Context
    
    init(context: Context) {
        self.context = context
    }
    
    func present(viewController: UIViewController) {
        switch self.context {
        case let window as UIWindow:
            guard window.rootViewController == nil else {
                preconditionFailure("Window can not have two root controllers")
            }
            
            window.rootViewController = viewController
            window.makeKeyAndVisible()
        
        case let rootViewController as UIViewController:
            rootViewController.present(viewController, animated: true)
            
        default:
            preconditionFailure("Currently undefined behaviour")
            
        }
    }
    
    func push(_ viewController: UIViewController) {
        switch self.context {
        case let navVC as UINavigationController:
            navVC.pushViewController(viewController, animated: true)
        
        default:
            preconditionFailure("Currently undefined behaviour")
            
        }
    }
    
    func presentEmbededInNavigationController(viewController: UIViewController) {
        switch self.context {
        case let rootViewController as UIViewController:
            rootViewController.present(UINavigationController(rootViewController: viewController), animated: true)
        
        default:
            preconditionFailure("Currently undefined behaviour")
            
        }
    }
    
    func pop() {
        switch self.context {
        case let navVC as UINavigationController:
            navVC.popViewController(animated: true)
        
        case let viewController as UIViewController:
            viewController.dismiss(animated: true)
            
        default:
            preconditionFailure("Currently undefined behaviour")
            
        }
    }
}

//
//  Coordinator.swift
//  RedditTestApp
//
//  Created by Serhii Oleinich on 07.02.2021.
//

import Foundation

protocol Coordinator {
    func start()
}

//
//  ApplicationCoordinator.swift
//  RedditTestApp
//
//  Created by Serhii Oleinich on 07.02.2021.
//

import Foundation

final class ApplicationCoordinator {
    private let assembly: ApplicationAssembly
    
    init(assembly: ApplicationAssembly) {
        self.assembly = assembly
    }
}

// MARK: - Coordiantor

extension ApplicationCoordinator: Coordinator {
    func start() {
        self.assembly.windowTransitionContainer.present(viewController: self.assembly.rootViewController)
        self.assembly.topListCoordinator().start()
    }
}

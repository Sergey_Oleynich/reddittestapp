//
//  TopListCoordinator.swift
//  RedditTestApp
//
//  Created by Serhii Oleinich on 07.02.2021.
//

import Foundation

final class TopListCoordinator {
    private let assembly: TopListAssembly
    
    init(assembly: TopListAssembly) {
        self.assembly = assembly
    }
}

// MARK: - Coordiantor

extension TopListCoordinator: Coordinator {
    func start() {
        var moduleInput = self.assembly.topListModuleInput()
        moduleInput.router = self
        
        self.assembly.flowTransitionContainer().push(moduleInput.viewInput!.viewController)
    }
}

// MARK: -

extension TopListCoordinator: TopListRouter {
    func navigateTo(_ action: TopListNavigationAction) {
        switch action {
        case .imageViewer(let url):
            var imageViewerModuleInput = self.assembly.imageViewerAssembly.rootModuleInput()
            imageViewerModuleInput.router = self
            imageViewerModuleInput.provideImage(for: url)
            
            self.assembly
                .modalTransitionContainer(context: self.assembly.responder())
                .presentEmbededInNavigationController(viewController: imageViewerModuleInput.viewInput!.viewController)
        }
    }
}

// MARK: -

extension TopListCoordinator: ImageViewerRouter {
    func navigateTo(_ action: ImageViewerNavigationAction) {
        switch action {
        case .back:
            self.assembly.modalTransitionContainer(context: self.assembly.responder()).pop()
        }
    }
}

//
//  ImageViewerPresenter.swift
//  RedditTestApp
//
//  Created by Serhii Oleinich on 10.02.2021.
//

import Foundation
import UIKit

protocol ImageViewerViewOutput {
    func onViewDidLoad()
    
    func handleCancel()
    func handleSaveToGallery(completion: @escaping (() -> Void))
}

final class ImageViewerPresenter: ImageViewerModuleInput {
    weak var viewInput: ImageViewerViewInput?
    var router: ImageViewerRouter?
    
    private var dataSource: URL?
    
    // MARK: - ImageViewerModuleInput
    
    func provideImage(for url: URL) {
        self.dataSource = url
    }
}

// MARK: - ImageViewerViewOutput

extension ImageViewerPresenter: ImageViewerViewOutput {
    func onViewDidLoad() {
        if let dataSource = self.dataSource {
            let urlRequest = URLRequest.builder
                .set(\.url, value: dataSource)
                .build
            
            self.viewInput?.viewImage(by: urlRequest)
        }
    }
    
    func handleCancel() {
        self.router?.navigateTo(.back)
    }
    
    func handleSaveToGallery(completion: @escaping (() -> Void)) {
        URLSession.shared.downloadTask(with: URLRequest(url: self.dataSource!)) { localURL, response, error in
            
            guard let cache = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first, let url = localURL else {
                return
            }
            
            do {
                let file = cache.appendingPathComponent("\(UUID().uuidString).jpg")
                try FileManager.default.moveItem(atPath: url.path, toPath: file.path)
                let image = UIImage(contentsOfFile: file.path)
                
                UIImageWriteToSavedPhotosAlbum(image!, nil, nil, nil)
                
                DispatchQueue.main.async {
                    completion()
                }
            }
            catch {
                print(error.localizedDescription)
            }
        }.resume()
        
    }
}

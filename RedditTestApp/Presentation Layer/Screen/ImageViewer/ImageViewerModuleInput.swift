//
//  ImageViewerModuleInput.swift
//  RedditTestApp
//
//  Created by Serhii Oleinich on 10.02.2021.
//

import Foundation

protocol ImageViewerModuleInput {
    var viewInput: ImageViewerViewInput? { get set }
    var router: ImageViewerRouter? { get set }
    
    func provideImage(for url: URL)
}

protocol ImageViewerRouter {
    func navigateTo(_ action: ImageViewerNavigationAction)
}

enum ImageViewerNavigationAction {
    case back
}

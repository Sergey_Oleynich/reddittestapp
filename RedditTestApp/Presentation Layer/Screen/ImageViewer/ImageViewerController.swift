//
//  ImageViewerController.swift
//  RedditTestApp
//
//  Created by Serhii Oleinich on 10.02.2021.
//

import UIKit
import WebKit

protocol ImageViewerViewInput: ViewInput {
    var viewOutput: ImageViewerViewOutput! { get }
    
    func viewImage(by urlRequest: URLRequest)
}

final class ImageViewerController: UIViewController {
    var viewOutput: ImageViewerViewOutput!
    
    @IBOutlet private var wkWebView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupNavigationControllerIfNeeded()
        self.viewOutput.onViewDidLoad()
    }
}

// MARK: - ImageViewerController

extension ImageViewerController: ImageViewerViewInput {
    func viewImage(by urlRequest: URLRequest) {
        self.wkWebView.load(urlRequest)
    }
}

// MARK: - Setup

extension ImageViewerController {
    func setupNavigationControllerIfNeeded() {
        guard self.navigationController != nil else { return }
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .cancel,
            target: self,
            action: #selector(leftBarButtonItemTouchUpInside(_:))
        )
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .action,
            target: self,
            action: #selector(rightBarButtonItemTouchUpInside(_:))
        )
    }
}

// MARK: - Actions

private extension ImageViewerController {
    @IBAction func leftBarButtonItemTouchUpInside(_ sender: Any) {
        self.viewOutput.handleCancel()
    }
    
    @IBAction func rightBarButtonItemTouchUpInside(_ sender: Any) {
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.rightBarButtonItem?.isLoading = true
        
        self.viewOutput.handleSaveToGallery {[weak self] in
            self?.navigationItem.rightBarButtonItem?.isLoading = false
            self?.setupNavigationControllerIfNeeded()
            
            self?.leftBarButtonItemTouchUpInside(sender)
        }
    }
}

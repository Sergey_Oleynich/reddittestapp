//
//  TopListViewModelProvider.swift
//  RedditTestApp
//
//  Created by Serhii Oleinich on 09.02.2021.
//

import Foundation

protocol TopListViewModelProvider {
    func provideViewModels(for response: RedditListingResponse) -> [TopListItemViewModel]
}

struct TopListViewModelProviderImpl: TopListViewModelProvider {
    private let dateComponentsFormatter: DateComponentsFormatter
    
    init(dateComponentsFormatter: DateComponentsFormatter) {
        self.dateComponentsFormatter = dateComponentsFormatter
    }
    
    func provideViewModels(for response: RedditListingResponse) -> [TopListItemViewModel] {
        response.data.children
            .unique
            .map(self.provideViewModel(for:))
    }
}

// MARK: - Private

private extension TopListViewModelProviderImpl {
    func provideViewModel(for model: RedditListItem) -> TopListItemViewModel {
        let dataModel = model.data
        return TopListItemViewModel(
            id: dataModel.fullId,
            title: dataModel.title,
            authorName: dataModel.author,
            commentsCount: "TopListComment".pluralLocalized(comment: "%@ comments", arg: dataModel.commentsCount),
            createdTimeStampt: dataModel.createdAt,
            thumbnailURL: dataModel.thumbnail,
            previewImageURL: dataModel.previewImagePayload?.images?.first?.source.url,
            dateComponentsFormatter: self.dateComponentsFormatter
        )
    }
}

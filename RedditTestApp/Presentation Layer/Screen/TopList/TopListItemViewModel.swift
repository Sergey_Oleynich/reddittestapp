//
//  TopListItemViewModel.swift
//  RedditTestApp
//
//  Created by Serhii Oleinich on 09.02.2021.
//

import Foundation

struct TopListItemViewModel {
    let id: String
    let title: String
    let authorName: String
    let commentsCount: String
    let createdTimeStampt: TimeInterval
    let thumbnailURL: URL?
    let previewImageURL: URL?
    
    private let dateComponentsFormatter: DateComponentsFormatter
    
    init(id: String, title: String, authorName: String, commentsCount: String, createdTimeStampt: TimeInterval, thumbnailURL: URL?, previewImageURL: URL?, dateComponentsFormatter: DateComponentsFormatter) {
        self.id = id
        self.title = title
        self.authorName = authorName
        self.commentsCount = commentsCount
        self.createdTimeStampt = createdTimeStampt
        self.thumbnailURL = thumbnailURL
        self.previewImageURL = previewImageURL
        self.dateComponentsFormatter = dateComponentsFormatter
    }
    
    func timeAfterCreation(completion: @escaping ((String, TopListItemViewModel) -> Void)) {
        DispatchQueue.global(qos: .userInitiated).async {
            let currentTime = Date().timeIntervalSince1970
            guard var formattedString = self.dateComponentsFormatter.string(from: currentTime - createdTimeStampt) else {
                DispatchQueue.main.sync {
                    completion("Unknown".localized, self)
                }
                return
            }

            formattedString = formattedString + "TopList.time.ago".localized
            DispatchQueue.main.sync {
                completion(formattedString, self)
            }
        }
        
    }
}

// MARK: - Hashable

extension TopListItemViewModel: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(self.id)
    }
    
    static func == (lhs: TopListItemViewModel, rhs: TopListItemViewModel) -> Bool {
        return lhs.id == rhs.id
    }
}

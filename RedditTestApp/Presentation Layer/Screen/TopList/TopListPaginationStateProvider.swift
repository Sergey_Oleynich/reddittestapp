//
//  TopListPaginationStateProvider.swift
//  RedditTestApp
//
//  Created by Serhii Oleinich on 10.02.2021.
//

import Foundation

final class TopListPaginationStateProvider {
    enum State {
        case setup
        case processable
        case end
    }
    
    enum Event {
        case after(itemId: String?)
        case before(itemId: String?)
    }
    
    var isProcessing: Bool = false
    
    private(set) var currentState: State = { .setup }()
    
    func refresh() {
        self.currentState = .setup
    }
    
    func apply(model: RedditListingItem?) {
        guard let model = model else {
            self.currentState = .end
            return
        }
        
        switch (model.after, model.before) {
        case (let after, nil):
            self.transition(with: .after(itemId: after))
            
        case (nil, let before):
            self.transition(with: .before(itemId: before))
            
        case (_, _):
            print("Unexpected behaviour")
            self.currentState = .end
            
        }
    }
    
    private func transition(with event: Event) {
        switch (currentState, event) {
        case (.setup, .after(let itemId)) where itemId != nil:
            self.currentState = .processable
            
        case (.setup, .after(let itemId)) where itemId == nil:
            self.currentState = .end
            
        case (.processable, .after(let itemId)) where itemId != nil:
            self.currentState = .processable
        
        case (.processable, .after(let itemId)) where itemId == nil:
            self.currentState = .end
            
        default:
            preconditionFailure("Need to implement")
            break
            
        }
    }
}

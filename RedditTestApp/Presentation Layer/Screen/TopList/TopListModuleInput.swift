//
//  TopListModuleInput.swift
//  RedditTestApp
//
//  Created by Serhii Oleinich on 08.02.2021.
//

import Foundation

protocol TopListModuleInput {
    var viewInput: TopListViewInput? { get set }
    var router: TopListRouter? { get set }
}

protocol TopListRouter {
    func navigateTo(_ action: TopListNavigationAction)
}

enum TopListNavigationAction {
    case imageViewer(url: URL)
}

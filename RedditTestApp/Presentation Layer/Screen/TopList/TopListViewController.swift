//
//  TopListViewController.swift
//  RedditTestApp
//
//  Created by Serhii Oleinich on 07.02.2021.
//

import UIKit

protocol ViewInput: AnyObject {
    var viewController: UIViewController { get }
}

extension ViewInput where Self: UIViewController {
    var viewController: UIViewController { self }
}

protocol TopListViewInput: ViewInput {
    var viewOutput: TopListViewOutput! { get }
    
    func onRequest(items: [TopListItemViewModel])
    func onRefresh(items: [TopListItemViewModel])
}

final class TopListViewController: UITableViewController {
    var viewOutput: TopListViewOutput!
    
    //@IBOutlet private weak var tableView: UITableView!
    
    private var dataSource: [TopListItemViewModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem()
        self.setupPullToRefresh()
        
        self.navigationItem.rightBarButtonItem?.isLoading = true
        self.viewOutput.onViewDidLoad()
    }
}

// MARK: - Actions

extension TopListViewController {
    @objc func refreshList(_ refreshControl: UIRefreshControl) {
        guard let latestItem = self.dataSource.first else { return }
        
        self.viewOutput.refresh(before: latestItem.id)
    }
}

// MARK: - TopListViewInput

extension TopListViewController: TopListViewInput {
    func onRequest(items: [TopListItemViewModel]) {
        self.navigationItem.rightBarButtonItem?.isLoading = false
        
        self.dataSource = (self.dataSource + items).unique
        self.tableView.reloadData()
    }
    
    func onRefresh(items: [TopListItemViewModel]) {
        self.navigationItem.rightBarButtonItem?.isLoading = false
        
        self.dataSource = items
        self.refreshControl?.endRefreshing()
        self.tableView.reloadData()
    }
}

// MARK: - UITableViewDataSource

extension TopListViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSource.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: TopListTableViewCell.self), for: indexPath) as? TopListTableViewCell else {
            preconditionFailure("Could not get cell")
        }
        
        cell.viewModel = self.dataSource[indexPath.row]
        cell.onThumbnailDidTouch = {[weak self] url in
            guard let self = self else { return }
            
            self.viewOutput.didTouchPreviewImage(url: url)
        }
        return cell
    }
}

// MARK: - UITableViewDelegate

extension TopListViewController {
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if self.viewOutput.shouldProvideNextBulk && indexPath.row == self.dataSource.count - 1 {
            let viewModel = self.dataSource[indexPath.row]
            
            self.navigationItem.rightBarButtonItem?.isLoading = true
            
            self.viewOutput.provideBulk(after: viewModel.id)
        }
    }
}

// MARK: - Private

private extension TopListViewController {
    func setupPullToRefresh() {
        self.refreshControl?.addTarget(self, action: #selector(refreshList(_:)), for: .valueChanged)
    }
}

//
//  TopListPresenter.swift
//  RedditTestApp
//
//  Created by Serhii Oleinich on 08.02.2021.
//

import Foundation

protocol TopListViewOutput {
    var shouldProvideNextBulk: Bool { get }
    
    func onViewDidLoad()
    func provideBulk(after itemId: String)
    func refresh(before itemId: String)
    func didTouchPreviewImage(url: URL)
}

final class TopListPresenter: TopListModuleInput {
    weak var viewInput: TopListViewInput?
    var router: TopListRouter?
    
    private let networkService: NetworkService
    private let resourceFactory: TopListResourceFactory
    private let viewModelProvider: TopListViewModelProvider
    private let paginationStateProvider: TopListPaginationStateProvider
    
    init(networkService: NetworkService,
         resourceFactory: TopListResourceFactory,
         viewModelProvider: TopListViewModelProvider,
         paginationStateProvider: TopListPaginationStateProvider = .init()) {
        self.networkService = networkService
        self.resourceFactory = resourceFactory
        self.viewModelProvider = viewModelProvider
        self.paginationStateProvider = paginationStateProvider
    }
}

// MARK: - TopListViewOutput

extension TopListPresenter: TopListViewOutput {
    var shouldProvideNextBulk: Bool {
        self.paginationStateProvider.currentState == .processable ||
        self.paginationStateProvider.currentState == .setup
    }
    
    func onViewDidLoad() {
        if self.shouldProvideNextBulk {
            self.request(resource: self.resourceFactory.listingResource(afterItem: nil))
        }
    }
    
    func provideBulk(after itemId: String) {
        guard !self.paginationStateProvider.isProcessing else { return }
        
        self.request(resource: self.resourceFactory.listingResource(afterItem: itemId))
    }
    
    func refresh(before itemId: String) {
        self.paginationStateProvider.refresh()
        
        guard !self.paginationStateProvider.isProcessing else { return }
        
        self.paginationStateProvider.isProcessing = true
        
        let resources = [
            self.resourceFactory.listingResource(beforeItem: itemId),
            self.resourceFactory.listingResource(afterItem: itemId)
        ]
        
        self.networkService.request(resources, responseQueue: .global()) {[weak self] result in
            defer { self?.paginationStateProvider.isProcessing = false }
            
            guard let self = self else { return }
            
            let error = result.compactMap { $0.error }
            
            guard error.isEmpty else {
                preconditionFailure("Need to process errors: \(error)")
            }
            
            DispatchQueue.main.async {
                if let response = result.first(where: { !($0.value?.data.after?.isEmpty ?? true)})?.value {
                    self.paginationStateProvider.apply(model: response.data)
                }
                
                self.viewInput?.onRefresh(
                    items: result
                        .compactMap({ $0.value })
                        .flatMap(self.viewModelProvider.provideViewModels(for:))
                )
            }
        }
    }
    
    func didTouchPreviewImage(url: URL) {
        self.router?.navigateTo(.imageViewer(url: url))
    }
}

// MARK: - Private

private extension TopListPresenter {
    func request(resource listingResource: Resource<RedditListingResponse>) {
        self.paginationStateProvider.isProcessing = true
        self.networkService.request(listingResource) {[weak self] result in
            defer {
                self?.paginationStateProvider.isProcessing = false
            }
            
            guard let self = self else { return }
            
            switch result {
            case .success(let response):
                self.paginationStateProvider.apply(model: response.data)
                self.viewInput?.onRequest(items: self.viewModelProvider.provideViewModels(for: response))
                
            case .failure(let error):
                preconditionFailure("Need to implement\nerror: \(error)")
                
            }
        }
    }
}

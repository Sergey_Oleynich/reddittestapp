//
//  TopListTableViewCell.swift
//  RedditTestApp
//
//  Created by Serhii Oleinich on 09.02.2021.
//

import UIKit

final class TopListTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var commentsLabel: UILabel!
    @IBOutlet private weak var authorNameLabel: UILabel!
    @IBOutlet private weak var authorImageView: UIImageView!
    @IBOutlet private weak var timeLabel: UILabel!
    
    var viewModel: TopListItemViewModel? {
        didSet {
            guard let viewModel = viewModel else { return }
            
            self.render(viewModel)
        }
    }
    let service = RESTService(responseHandler: RESTServiceErrorResponseHandler())
    var onThumbnailDidTouch: ((URL) -> Void)?
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.authorImageView.image = nil
        self.showThumbnailImageView()
    }
}

// MARK: - Actions

private extension TopListTableViewCell {
    @IBAction func thumbnailTouchUpInside(sender: UIButton) {
        guard let url = self.viewModel?.previewImageURL else {
            return
        }
        
        self.onThumbnailDidTouch?(url)
    }
}

// MARK: - Private

private extension TopListTableViewCell {
    func render(_ viewModel: TopListItemViewModel) {
        self.titleLabel.text = viewModel.title
        self.authorNameLabel.text = viewModel.authorName
        self.commentsLabel.text = viewModel.commentsCount
        
        viewModel.timeAfterCreation {[weak self] time, item in
            guard let self = self else { return }
            
            if item == self.viewModel {
                self.timeLabel.text = time
            }
        }
        
        if let url = viewModel.thumbnailURL {
            self.load(url: url, item: viewModel) {[weak self] item, image in
                guard let self = self else { return }
                
                guard item == self.viewModel else { return }
                
                switch image {
                case .some(let image):
                    self.authorImageView.image = image
                    self.showThumbnailImageView()
                    
                case .none:
                    self.hideThumbnailImageView()
                    
                }
            }
        }
    }
    
    func showThumbnailImageView() {
        if self.authorImageView.isHidden == false { return }
        
        self.authorImageView.isHidden = false
        self.authorImageView.superview?.superview?.isHidden = false
    }
    
    func hideThumbnailImageView() {
        if self.authorImageView.isHidden == true { return }
        
        self.authorImageView.isHidden = true
        self.authorImageView.superview?.superview?.isHidden = true
    }
    
    
    
    func load(url: URL, item: TopListItemViewModel, completion: @escaping (TopListItemViewModel, UIImage?) -> ()) {
        DispatchQueue.global().async { [weak self] in
            guard let self = self else { return }
            
            guard url.isValid else {
                DispatchQueue.main.sync {
                    self.hideThumbnailImageView()
                }
                return
            }
            
            guard let cache = FileManager.default.urls(
                    for: .cachesDirectory,
                    in: .userDomainMask)
                    .first else {
                return
            }
            
            let fileUrl = cache.appendingPathComponent("\(url.lastPathComponent)")
            
            if FileManager.default.fileExists(atPath: fileUrl.path) {
                let image = UIImage(contentsOfFile: fileUrl.path)
                
                DispatchQueue.main.async {
                    completion(item, image)
                }
            }
            else {
                let resource: Resource<URLResponseModel> = Resource<URLResponseModel>(request: URLRequest(url: url))
                self.service.request(resource, responseQueue: .main) { result in
                    switch result {
                    case .success(let model):
                        print("Image was saved")
                        try? FileManager.default.moveItem(atPath: model.url!.path, toPath: fileUrl.path)
                        let image = UIImage(contentsOfFile: fileUrl.path)
                        DispatchQueue.main.async {
                            completion(item, image)
                        }
                        
                    case .failure(let error):
                        print("Image error = \(error)")
                        DispatchQueue.main.async {
                            completion(item, nil)
                        }
                    }
                }
            }
        }
    }
}

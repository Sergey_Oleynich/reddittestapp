//
//  RedditListingModel.swift
//  RedditTestApp
//
//  Created by Serhii Oleinich on 08.02.2021.
//

import Foundation

struct RedditListingResponse: Decodable, Response {
    static var responseType: Data.Type = Data.self
    
    let data: RedditListingItem
}

struct RedditListingItem: Decodable {
    let children: [RedditListItem]
    let after: String?
    let before: String?
}

struct RedditListItem: Decodable, Hashable {
    let data: RedditListContent
    let kind: String
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(data)
    }
    
    static func == (lhs: RedditListItem, rhs: RedditListItem) -> Bool {
        return lhs.data == rhs.data
    }
}

struct RedditListContent: Decodable, Hashable {
    let id: String
    let fullId: String
    let title: String
    let author: String
    let commentsCount: Int
    let thumbnail: URL?
    let createdAt: TimeInterval
    let previewImagePayload: RedditListPreviewImagePayload?
    
    enum CodingKeys: String, CodingKey {
        case id
        case fullId = "name"
        case title
        case author
        case thumbnail
        case createdAt = "created_utc"
        case commentsCount = "num_comments"
        case previewImagePayload = "preview"
    }
    
    // MARK: - Hashable
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(fullId)
    }
    
    static func == (lhs: RedditListContent, rhs: RedditListContent) -> Bool {
        return lhs.fullId == rhs.fullId
    }
}

struct RedditListPreviewImagePayload: Decodable {
    let images: [RedditListPreviewImage]?
}

struct RedditListPreviewImage: Decodable {
    let source: RedditListPreviewImageData
}

struct RedditListPreviewImageData: Decodable {
    let url: URL?
    let width: Double
    let height: Double
    
    enum CodingKeys: String, CodingKey {
        case url
        case width
        case height
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        let absoluteString = try values.decode(String.self, forKey: .url).replacingOccurrences(of: "&amp;", with: "&")
        self.url = URL(string: absoluteString)
        self.width = try values.decode(Double.self, forKey: .width)
        self.height = try values.decode(Double.self, forKey: .height)
    }
}

struct URLResponseModel: Decodable, Hashable, Response {
    static var responseType: URL.Type = URL.self
    
    let url: URL?
}

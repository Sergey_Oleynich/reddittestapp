//
//  NetworkService.swift
//  RedditTestApp
//
//  Created by Serhii Oleinich on 08.02.2021.
//

import Foundation

enum HTTPMethod: String {
    case get = "GET"
    
    init(value: String?) {
        switch value {
        case .some(let method) where HTTPMethod(rawValue: method.capitalized) != nil:
            self = HTTPMethod(rawValue: method.capitalized)!
            
        default: self = .get
        }
    }
}

protocol NetworkService {
    func request<ResponseModel>(
        _ resource: Resource<ResponseModel>,
        responseQueue: DispatchQueue,
        response: @escaping (Result<ResponseModel, Error>) -> Void
    )
    
    func request<ResponseModel>(
        _ resources: [Resource<ResponseModel>],
        responseQueue: DispatchQueue,
        response: @escaping ([Result<ResponseModel, Error>]) -> Void
    )
}

extension NetworkService {
    func request<ResponseModel>(
        _ resource: Resource<ResponseModel>,
        response: @escaping (Result<ResponseModel, Error>) -> Void
    ) {
        self.request(resource, responseQueue: .main, response: response)
    }
}

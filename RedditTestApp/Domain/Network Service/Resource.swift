//
//  Resource.swift
//  RedditTestApp
//
//  Created by Serhii Oleinich on 08.02.2021.
//

import Foundation

protocol Response {
    associatedtype ResponseType
    
    static var responseType: ResponseType.Type { get }
}

struct Resource<ResponseModel: Response> where ResponseModel: Decodable {
    let request: URLRequest
    let decoder: (ResponseModel.ResponseType?) -> Result<ResponseModel, Error>
}

extension Resource where ResponseModel.ResponseType == Data {
    init(request: URLRequest) {
        self.request = request
        
        self.decoder = { data -> Result<ResponseModel, Error> in
            guard let data = data, !data.isEmpty else {
                preconditionFailure("Need to implement")
            }
            
            let decoder = JSONDecoder()
            return Result { try decoder.decode(ResponseModel.self, from: data) }
        }
    }
}

extension URL: Response {
    static var responseType: URL.Type { URL.self }
}

extension Resource where ResponseModel.ResponseType == URL {
    init(request: URLRequest) {
        self.request = request
        
        self.decoder = { url -> Result<ResponseModel, Error> in
            guard let imageUrl = url else {
                preconditionFailure("sdfsdfsdf")
                //return .failure(<#T##Error#>)
            }
         
            do {
                guard let cache = FileManager.default.urls(
                        for: .cachesDirectory,
                        in: .userDomainMask)
                        .first else {
                    preconditionFailure("sdfsdf")
                }
                
                let fileUrl = cache.appendingPathComponent("\(imageUrl.lastPathComponent)")
                try FileManager.default.moveItem(atPath: imageUrl.path, toPath: fileUrl.path)
                
                let rrr = ["url": fileUrl]
                let rrrdata = try? JSONEncoder().encode(rrr)
                
                return Result { try JSONDecoder().decode(ResponseModel.self, from: rrrdata!) }
                
            } catch let error {
                preconditionFailure(error.localizedDescription)
            }
            
        }
    }
}

/*
 struct Resource<DecoderModel: Decoder> {
     let request: URLRequest
     //let decoder: (Data?) -> Result<ResponseModel, Error>
     let decoder1: DecoderModel
     
 //    init<D: DecoderModel>(request: URLRequest, decoder1: D) where D.ResponseModel == ResponseModel {
 //        self.request = request
 //        self.decoder1 = decoder1
 //    }
 }

 //extension Resource {
 //    init(request: URLRequest) {
 //        self.request = request
 //    }
 //
 //    init(request: URLRequest, dataDecoder: DataDecoder<ResponseModel>) {
 //        self.request = request
 //
 //        self.decoder1 = dataDecoder
 //    }
 //}

 protocol Decoder {
     associatedtype ResponseModel
 //    associatedtype ResponseData
     
     func decode(data: Data?) -> Result<ResponseModel, Error>
 }

 struct AnyDecoder<ResponseModel>: Decoder {
     
     private let decoder: (Data?) -> Result<ResponseModel, Error>
     
     init<D: Decoder>(decoder: D) where D.ResponseModel == ResponseModel {
         self.decoder = { data in
             decoder.decode(data: data)
         }
     }
     
     func decode(data: Data?) -> Result<ResponseModel, Error> {
         return decoder(data)
     }
 }

 struct DataDecoder<ResponseModel>: Decoder where ResponseModel: Decodable {
     func decode(data: Data?) -> Result<ResponseModel, Error> {
         guard let data = data, !data.isEmpty else {
             preconditionFailure("Need to implement")
         }
         
         let decoder = JSONDecoder()
         return Result { try decoder.decode(ResponseModel.self, from: data) }
     }
 }
 */

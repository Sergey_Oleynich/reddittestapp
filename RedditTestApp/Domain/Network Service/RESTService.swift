//
//  RESTService.swift
//  RedditTestApp
//
//  Created by Serhii Oleinich on 08.02.2021.
//

import Foundation
import UIKit

struct RESTService: NetworkService {
    private let session: URLSession
    private let responseHandler: RESTServiceResponseHandler
    
    init(session: URLSession = .shared, responseHandler: RESTServiceResponseHandler) {
        self.session = session
        self.responseHandler = responseHandler
    }
    
    func request<ResponseModel>(
        _ resource: Resource<ResponseModel>,
        responseQueue: DispatchQueue,
        response: @escaping (Result<ResponseModel, Error>) -> Void
    ) where ResponseModel.ResponseType == URL {
        self.session.downloadTask(with: resource.request) { url, httpResponse, error in
            let result = Result { try self.responseHandler.handle(response: (nil, httpResponse, error)) }
            
            switch url {
            case .some(let url):
                let result = resource.decoder(url)
                responseQueue.async {
                    response(result)
                }
                
            case .none:
                preconditionFailure("need to implement")
//                responseQueue.async {
//                    response(result)
//                }
            }
            
        }.resume()
    }
    
    func request<ResponseModel>(
        _ resource: Resource<ResponseModel>,
        responseQueue: DispatchQueue,
        response: @escaping (Result<ResponseModel, Error>) -> Void
    ) {
        self.session.dataTask(with: resource.request) { data, httpResponse, error in
            let result = Result { try self.responseHandler.handle(response: (data, httpResponse, error)) }
            
            switch result {
            case .success(let data):
                guard let data = data as? ResponseModel.ResponseType else {
                    preconditionFailure("Need to implement")
                }
                
                let result = resource.decoder(data)
                responseQueue.async {
                    response(result)
                }
                
            case .failure(let error):
                responseQueue.async {
                    response(.failure(error))
                }
            }
            
        }.resume()
    }
    
    func request<ResponseModel>(
        _ resources: [Resource<ResponseModel>],
        responseQueue: DispatchQueue,
        response: @escaping ([Result<ResponseModel, Error>]) -> Void
    ) {
        let group = DispatchGroup()
        
        var results: [URLRequest: Result<ResponseModel, Error>?] = [:]
        
        resources.forEach { resource in
            results[resource.request] = .none
            group.enter()
            request(resource, responseQueue: .global()) { requestResult in
                results[resource.request] = requestResult
                group.leave()
            }
        }
        
        group.notify(queue: .global()) {
            let arrayResults: [Result<ResponseModel, Error>] = results.compactMap { $0.value }
            
            responseQueue.async {
                response(arrayResults)
            }
        }
    }
}

extension UIDevice {
    func systemVersionIsLessThan(version: String) -> Bool {
        return self.systemVersion.compare(version, options: .numeric) == .orderedAscending
    }
}

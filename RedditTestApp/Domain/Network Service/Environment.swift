//
//  Environment.swift
//  RedditTestApp
//
//  Created by Serhii Oleinich on 09.02.2021.
//

import Foundation

protocol Environment {
    func configuration(_ key: Bundle.Key) -> String
}

struct EnvironmentImpl: Environment {
    fileprivate let bundle = Bundle.main

    func configuration(_ key: Bundle.Key) -> String {
        guard let result: String = bundle.info(forKey: key) else {
            preconditionFailure("Need to provide value for key \(key)")
        }
        
        return result
    }
}

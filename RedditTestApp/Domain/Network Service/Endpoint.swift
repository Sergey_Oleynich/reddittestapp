//
//  Endpoint.swift
//  RedditTestApp
//
//  Created by Serhii Oleinich on 09.02.2021.
//

import Foundation

enum Endpoint {
    enum Top: String {
        enum Query: String {
            case after
            case before
        }
        
        case path = "/top.json"
    }
}

//
//  RESTServiceResponseHandler.swift
//  RedditTestApp
//
//  Created by Serhii Oleinich on 08.02.2021.
//

import Foundation

protocol RESTServiceResponseHandler {
    var next: RESTServiceResponseHandler? { get set }
    
    func handle(response: (data: Data?, httpResponse: URLResponse?, error: Error?)) throws -> Data?
}

final class RESTServiceErrorResponseHandler: RESTServiceResponseHandler {
    var next: RESTServiceResponseHandler?
    
    func handle(response: (data: Data?, httpResponse: URLResponse?, error: Error?)) throws -> Data? {
        switch response.error {
        case .some(let error):
            throw error
            
        case .none:
            return try next?.handle(response: response)
        }
    }
}

final class RESTServiceHTTPResponseHandler: RESTServiceResponseHandler {
    var next: RESTServiceResponseHandler?
    
    func handle(response: (data: Data?, httpResponse: URLResponse?, error: Error?)) throws -> Data? {
        switch response.httpResponse {
        case .some(let httpResponse as HTTPURLResponse) where (200...300).contains(httpResponse.statusCode):
            return try next?.handle(response: response)
            
        default:
            preconditionFailure("Need to implement")
            
        }
    }
}

final class RESTServiceDataResponseHandler: RESTServiceResponseHandler {
    var next: RESTServiceResponseHandler?
    
    func handle(response: (data: Data?, httpResponse: URLResponse?, error: Error?)) throws -> Data? {
        return response.data
    }
}

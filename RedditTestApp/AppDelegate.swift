//
//  AppDelegate.swift
//  RedditTestApp
//
//  Created by Serhii Oleinich on 07.02.2021.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    lazy var window: UIWindow? = UIWindow(frame: UIScreen.main.bounds)

    private lazy var appCoordinator: Coordinator = {
        guard let window = window else { preconditionFailure("Could not retrieve window") }
        
        let assembly = ApplicationAssemblyImpl(mainWindow: window)
        return assembly.flowCoordinator()
    }()
    
    private lazy var stateRestorationManager: StateRestorationManager = StateRestorationManagerImpl()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        appCoordinator.start()
        return true
    }
    
    func application(_ application: UIApplication, shouldSaveSecureApplicationState coder: NSCoder) -> Bool {
        return self.stateRestorationManager.shouldSaveApplicationState
    }
    
    func application(_ application: UIApplication, shouldRestoreApplicationState coder: NSCoder) -> Bool {
        return self.stateRestorationManager.shouldRestoreApplicationState
    }
    
    func application(_ application: UIApplication, didDecodeRestorableStateWith coder: NSCoder) {
        
    }
}


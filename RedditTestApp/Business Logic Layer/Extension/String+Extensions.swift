//
//  String+Extensions.swift
//  RedditTestApp
//
//  Created by Serhii Oleinich on 09.02.2021.
//

import Foundation

extension String {
    var localized: String { NSLocalizedString(self, comment: "") }
    
    func pluralLocalized(comment: String, arg: CVarArg) -> String {
        String.localizedStringWithFormat(NSLocalizedString(self, comment: comment), arg)
    }
}

//
//  URLRequest+Extensions.swift
//  RedditTestApp
//
//  Created by Serhii Oleinich on 08.02.2021.
//

import Foundation

extension URLRequest: Builder {
    init() { self.init(url: URL(string: "www.apple.com")!) }
}

extension URLRequest {
    var method: HTTPMethod {
        get { HTTPMethod(value: httpMethod) }
        set { self.httpMethod = newValue.rawValue }
    }
}

extension URL {
    static private let urlDetector: NSDataDetector? = try? NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
    
    var isValid: Bool {
        guard let detector = URL.urlDetector else { return true }
        
        guard let match = detector.firstMatch(in: self.absoluteString, options: [], range: NSRange(location: 0, length: self.absoluteString.utf16.count)) else {
            return false
        }
        
        return match.range.length == self.absoluteString.utf16.count
    }
}

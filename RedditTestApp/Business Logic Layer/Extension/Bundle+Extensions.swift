//
//  Bundle+Extensions.swift
//  RedditTestApp
//
//  Created by Serhii Oleinich on 09.02.2021.
//

import Foundation

extension Bundle {
    enum Key: String {
        case schemeURL = "SCHEME_URL"
        case hostURL = "HOST_URL"
    }
}

extension Bundle {
    func info<T>(forKey key: Bundle.Key) -> T? {
        return infoDictionary?[key.rawValue] as? T
    }
}

//
//  Array+Extensions.swift
//  RedditTestApp
//
//  Created by Serhii Oleinich on 10.02.2021.
//

import Foundation

extension Array where Element: Hashable {
    var unique: [Element] {
        var set: Set<Element> = []
        return reduce([], { partialResult, element -> [Element] in
            guard !set.contains(element) else { return partialResult }
            set.insert(element)
            return partialResult + [element]
        })
    }
}

//
//  Result+Extensions.swift
//  RedditTestApp
//
//  Created by Serhii Oleinich on 10.02.2021.
//

import Foundation

extension Result {
    var value: Success? {
        if case let .success(value) = self { return value }
        
        return nil
    }
    
    var error: Failure? {
        if case let .failure(error) = self { return error }
        
        return nil
    }
}

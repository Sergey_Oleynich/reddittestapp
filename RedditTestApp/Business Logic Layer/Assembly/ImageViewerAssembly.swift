//
//  ImageViewerAssembly.swift
//  RedditTestApp
//
//  Created by Serhii Oleinich on 10.02.2021.
//

import Foundation
import UIKit

protocol ImageViewerAssembly {
    func rootModuleInput() -> ImageViewerModuleInput
}

struct ImageViewerAssemblyImpl {
    
}

// MARK: - ImageViewerAssembly

extension ImageViewerAssemblyImpl: ImageViewerAssembly {
    func rootModuleInput() -> ImageViewerModuleInput {
        guard let viewController = UIStoryboard(name: "Common", bundle: nil).instantiateViewController(withIdentifier: String(describing: ImageViewerController.self)) as? ImageViewerController else { preconditionFailure("Could not get ImageViewerController") }
        
        let presenter = ImageViewerPresenter()
        
        viewController.viewOutput = presenter
        presenter.viewInput = viewController
        
        return presenter
    }
}

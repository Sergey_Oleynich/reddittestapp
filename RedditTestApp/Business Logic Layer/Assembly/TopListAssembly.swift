//
//  TopListAssembly.swift
//  RedditTestApp
//
//  Created by Serhii Oleinich on 07.02.2021.
//

import UIKit

protocol TopListAssembly {
    var imageViewerAssembly: ImageViewerAssembly { get }
    
    func flowTransitionContainer() -> TransitionContainer
    func modalTransitionContainer(context: ViewInput) -> TransitionContainer
    
    func flowCoordinator() -> TopListCoordinator
    func topListModuleInput() -> TopListModuleInput
    
    func responder() -> ViewInput
}

struct TopListAssemblyImpl {
    var imageViewerAssembly: ImageViewerAssembly { ImageViewerAssemblyImpl() }
    
    private let mainWindow: UIWindow
    
    init(mainWindow: UIWindow) {
        self.mainWindow = mainWindow
    }
}

// MARK: - TopListAssembly

extension TopListAssemblyImpl: TopListAssembly {
    func flowTransitionContainer() -> TransitionContainer {
        guard let navVC = self.mainWindow.rootViewController as? UINavigationController else {
            preconditionFailure("Could not retrieve flow transition container")
        }
        
        return TransitionContainerImpl<UINavigationController>(context: navVC)
    }
    
    func modalTransitionContainer(context: ViewInput) -> TransitionContainer {
        TransitionContainerImpl<UIViewController>(context: context.viewController)
    }
    
    func flowCoordinator() -> TopListCoordinator {
        TopListCoordinator(assembly: self)
    }
    
    func responder() -> ViewInput {
        guard let navVC = self.mainWindow.rootViewController as? UINavigationController else {
            preconditionFailure("Could not retrieve flow transition container")
        }
        
        guard let viewInput = navVC.visibleViewController as? ViewInput else {
            preconditionFailure("Could not retrieve view input")
        }
        
        return viewInput
    }
    
    func topListModuleInput() -> TopListModuleInput {
        guard let viewController = UIStoryboard(name: "TopList", bundle: nil).instantiateViewController(withIdentifier: String(describing: TopListViewController.self)) as? TopListViewController else {
            preconditionFailure("Could not retrieve controller from storyboard TopList")
        }
        
        let a1 = RESTServiceErrorResponseHandler()
        let a2 = RESTServiceHTTPResponseHandler()
        let a3 = RESTServiceDataResponseHandler()

        a1.next = a2
        a2.next = a3
        let networkService = RESTService(responseHandler: a1)
        
        let resourceFactory = TopListResourceFactoryImpl(environment: EnvironmentImpl())
        
        let presenter = TopListPresenter(
            networkService: networkService,
            resourceFactory: resourceFactory,
            viewModelProvider: TopListViewModelProviderImpl(dateComponentsFormatter: {
                let dateComponentsFormatter: DateComponentsFormatter = DateComponentsFormatter()
                    
                dateComponentsFormatter.allowedUnits = [.hour, .minute, .second]
                dateComponentsFormatter.unitsStyle = .full
                dateComponentsFormatter.maximumUnitCount = 1
                    
                return dateComponentsFormatter
            }())
        )
        
        presenter.viewInput = viewController
        viewController.viewOutput = presenter
        
        return presenter
    }
}

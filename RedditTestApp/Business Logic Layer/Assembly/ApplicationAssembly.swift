//
//  ApplicationAssembly.swift
//  RedditTestApp
//
//  Created by Serhii Oleinich on 07.02.2021.
//

import UIKit

protocol ApplicationAssembly {
    var windowTransitionContainer: TransitionContainer { get }
    var rootViewController: UIViewController { get }
    
    func flowCoordinator() -> ApplicationCoordinator
    func topListCoordinator() -> TopListCoordinator
}

struct ApplicationAssemblyImpl: ApplicationAssembly {
    private let mainWindow: UIWindow
    
    init(mainWindow: UIWindow) {
        self.mainWindow = mainWindow
    }
}

// MARK: - ApplicationAssembly

extension ApplicationAssemblyImpl {
    var windowTransitionContainer: TransitionContainer { TransitionContainerImpl<UIWindow>(context: self.mainWindow) }
    var rootViewController: UIViewController { self.mainWindow.rootViewController ?? UINavigationController() }
    
    func flowCoordinator() -> ApplicationCoordinator {
        let coordinator = ApplicationCoordinator(assembly: self)
        return coordinator
    }
    
    func topListCoordinator() -> TopListCoordinator {
        let assembly = TopListAssemblyImpl(mainWindow: mainWindow)
        let coordinator = TopListCoordinator(assembly: assembly)
        return coordinator
    }
}
 

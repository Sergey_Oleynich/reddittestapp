//
//  TopListResourceFactory.swift
//  RedditTestApp
//
//  Created by Serhii Oleinich on 08.02.2021.
//

import Foundation

protocol TopListResourceFactory {
    func listingResource(afterItem: String?) -> Resource<RedditListingResponse>
    func listingResource(beforeItem: String?) -> Resource<RedditListingResponse>
}

struct TopListResourceFactoryImpl {
    private let environment: Environment
    
    init(environment: Environment) {
        self.environment = environment
    }
}

// MARK: - TopListResourceFactory

extension TopListResourceFactoryImpl: TopListResourceFactory {
    func listingResource(afterItem: String?) -> Resource<RedditListingResponse> {
        self.listingResource(queryItems: [
            URLQueryItem(name: Endpoint.Top.Query.after.rawValue, value: afterItem)
        ].filter { $0.value != nil && $0.value?.isEmpty == false })
    }
    
    func listingResource(beforeItem: String?) -> Resource<RedditListingResponse> {
        self.listingResource(queryItems: [
            URLQueryItem(name: Endpoint.Top.Query.after.rawValue, value: beforeItem)
        ].filter { $0.value != nil && $0.value?.isEmpty == false })
    }
    
    private func listingResource(queryItems: [URLQueryItem]) -> Resource<RedditListingResponse> {
        let url = URLComponents.builder
            .set(\.scheme, value: self.environment.configuration(.schemeURL))
            .set(\.host, value: self.environment.configuration(.hostURL))
            .set(\.path, value: Endpoint.Top.path.rawValue)
            .set(\.queryItems, value: queryItems)
            .build
            .url
        
        let request = URLRequest.builder
            .set(\.method, value: .get)
            .set(\.url, value: url)
            .build
        
        return Resource<RedditListingResponse>(request: request)
    }
}

//
//  StateRestorationManager.swift
//  RedditTestApp
//
//  Created by Serhii Oleinich on 11.02.2021.
//

import Foundation

protocol StateRestorationManager {
    var shouldSaveApplicationState: Bool { get }
    var shouldRestoreApplicationState: Bool { get }
}

final class StateRestorationManagerImpl: StateRestorationManager {
    var shouldSaveApplicationState: Bool {
        UserDefaults.standard.setValue(true, forKey: UserDefaults.Keys.restoreApplicationState.rawValue)
        return true
    }
    
    var shouldRestoreApplicationState: Bool {
        defer {
            UserDefaults.standard.removeObject(forKey: UserDefaults.Keys.restoreApplicationState.rawValue)
        }
        
        return UserDefaults.standard.bool(forKey: UserDefaults.Keys.restoreApplicationState.rawValue)
    }
}

extension UserDefaults {
    enum Keys: String {
        case restoreApplicationState
    }
}

//
//  Director.swift
//  RedditTestApp
//
//  Created by Serhii Oleinich on 08.02.2021.
//

import Foundation

final class Director<Builder> {
    private var builder: Builder
    
    var build: Builder { builder }
    
    init(_ builder: Builder) {
        self.builder = builder
    }
    
    func set<Value>(_ keyPath: WritableKeyPath<Builder, Value>, value: Value) -> Director<Builder> {
        apply { $0.builder[keyPath: keyPath] = value }
    }
    
    @inline(__always) private func apply(block: (Self) -> ()) -> Self {
        block(self)
        return self
    }
}

protocol Initable {
    init()
}

protocol Builder: Initable {
    static var builder: Director<Self> { get }
}

extension Builder where Self: Initable {
    static var builder: Director<Self> { Director(Self()) }
}

//
//  Stack.swift
//  RedditTestApp
//
//  Created by Serhii Oleinich on 11.02.2021.
//

import Foundation

struct Stack<Element> {
    private var storage: [Element] = []
    
    mutating func push(_ element: Element) {
        storage.append(element)
    }
    
    @discardableResult
    mutating func pop() -> Element? {
        return storage.popLast()
    }
    
    func peek() -> Element? {
        return storage.last
    }
}

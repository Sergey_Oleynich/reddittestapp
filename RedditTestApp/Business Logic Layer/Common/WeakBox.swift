//
//  WeakBox.swift
//  RedditTestApp
//
//  Created by Serhii Oleinich on 11.02.2021.
//

import Foundation

final class WeakBox<Element> {
    var value: Element? {
        return self.internalValue as? Element
    }
    
    private weak var internalValue: AnyObject?
    
    init(_ value: Element) {
        self.internalValue = value as AnyObject
    }
}
